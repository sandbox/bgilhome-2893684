# Taxonomy Term Status

This module adds a 'status' base field to taxonomy terms to enable term publishing/unpublishing. On installation, all terms are (by default) published.

Permissions are added per vocabulary to view published/unpublished terms.

Code is heavily copied from the Thunder Taxonomy module, part of the Thunder distribution ((https://www.drupal.org/project/thunder).